<?php include('header.php') ?>

    <div class="page_title" style="background-image:url(img/header_quemsomos.jpg)">
        <div class="container">
            <div class="left">
                <h1>Sobre a CrediPronto</h1>
            </div>
            <div class="right">
                <span>
                    <i class="icon-telephone"></i> 3003-1144
                </span>
                <span>
                    <i class="icon-whatsapp"></i> (11)991161-8296
                </span>
            </div>
        </div>
    </div>

    <section class="container">
        <div class="content_sobre">
            <div class="left">
                <div class="heading">
                    <h2>A CrediPronto</h2>
                </div>
                <p>A CrediPronto é uma empresa de consultoria especializada em financiamento imobiliário. Foi
                    criada por meio da associação entre duas empresas que são referências no mercado
                    imobiliário e financeiro do Brasil: a Lopes e o Itaú.</p>
                    <p>
                        O Banco Itaú - uma das maiores instituições financeiras da América Latina - trouxe todo o
                        conhecimento e experiência em prestação de serviços financeiros para o setor imobiliário.
                    </p>
                    <p>
                        A Lopes - líder no mercado imobiliário e uma das empresas mais admiradas do Brasil - sabe
                        a importância que a compra de um imóvel tem na vida de seus clientes e dedica-se totalmente a essa conquista.
                    </p>
                    <p>
                        Presente em 12 estados e Distrito Federal, a CrediPronto conta com uma equipe de
                        consultores financeiros altamente capacitados para atender as necessidades de seus clientes
                        e encontrar a melhor solução de financiamento para seu imóvel. Além disso, a empresa está
                        preparada para cuidar de tudo com maior rapidez, desde a obtenção de documentos e
                        certidões até avaliação de valores do processo.
                    </p>
                </div>

                <div class="right">

                    <div class="table_sobre">
                        <ul>
                            <li>
                                <div class="head">
                                    <i class="icon-plus active"></i>
                                    <h2>
                                         Missão
                                    </h2>
                                </div>

                                <p style="display:block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Visão
                                    </h2>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Realização de desejos e necessidades de nossos clientes
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Excelência em serviços
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus "></i>
                                    <h2>
                                        Cultura
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                        </ul>
                    </div>
                    <div class="btn_container">
                        <button class="btn blue">
                            <i class="icon-check "></i> TRABALHE CONOSCO</button>
                    </div>
                </div>
            </div>

        </section>
        <script>
        $(document).ready(function(){
            $('.head').click(function(){
                $('.table_sobre ul li p').slideUp();
                $(this).parent().find('p').slideToggle();

                $('.table_sobre ul li i').parent().find('i').removeClass("active");
                $(this).parent().find('i').addClass("active");
            });
        });
        </script>

<?php include('footer.php') ?>