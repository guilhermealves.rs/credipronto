<?php include('header.php') ?>

<div class="pagina-404">
  
    <div class="heading">
        <div class="container">
            <h2>404 - PÁGINA NÃO ENCONTRADA.</h2>
        </div>
    </div>
    <div class="container">
    <h3>Não encontramos a página solicitada</h3>

    <p>Verifique abaixo alguns motivos que ocasionaram esse erro:</p>

    <ul class="lista-404">
        <li>Um endereço digitado errado;</li>
        <li>Uma página que não existe mais;</li>
        <li>Uma página que foi movida.</li>
    </ul>
    </div>
</div>

<?php include('footer.php') ?>