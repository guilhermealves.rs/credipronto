<?php include('header.php') ?>

    <div class="page_title" style="background-image:url(img/header_parceiros.jpg)">
        <div class="container">
            <div class="left">
                <h1>PARCEIROS</h1>
            </div>
            <div class="right">
                <span>
                    <i class="icon-telephone"></i> 3003-1144
                </span>
                <span>
                    <i class="icon-whatsapp"></i> (11)991161-8296
                </span>
            </div>
        </div>
    </div>

    <section class="container">
        <div class="content_parceiros">
            <div class="left">
                    <h1>Preencha o fomrulário abaixo e torne-se um parceiro CrediPronto</h1>

                    <form action="" class="form_parceiros">
                        <div class="content">
                            <div class="c6">
                                <input type="text" class="input" placeholder="Nome da Empresa">
                            </div>
                            <div class="c6">
                                <input type="text" class="input" placeholder="CNPJ">
                            </div>

                            <div class="c6">
                                <input type="text" class="input" placeholder="Nome do Responsável">
                            </div>
                            <div class="c6">
                                <input type="text" class="input" placeholder="CPF">
                            </div>

                            <div class="c4">
                                <input type="text" class="input" placeholder="CEP">
                            </div>
                            <div class="c4">
                                <input type="text" class="input" placeholder="E-mail">
                            </div>
                            <div class="c4">
                                <input type="text" class="input" placeholder="Telefone">
                            </div>

                            <div class="c6">
                                <select name="" id="">
                                    <option value="" selected>Como fico sabendo da CrediPronto?</option>
                                    <option value="">Google</option>
                                    <option value="">Google</option>
                                    <option value="">Google</option>
                                </select>
                            </div>
                            <div class="c6">

                            </div>

                        </div>

                        <div class="submit_container">
                            <button class="submit" type="submit" name="button">QUERO SER PARCEIRO</button>
                        </div>

                    </form>
            </div>


                <div class="right">
                        <h1>Vantagens de ser nosso parceiro:</h1>

                    <div class="table_sobre">
                        <ul>
                            <li>
                                <div class="head">
                                    <i class="icon-plus active"></i>
                                    <h2>
                                         Simulador de Financiamento Online
                                    </h2>
                                </div>

                                <p style="display:block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Portal de gestão de clientes
                                    </h2>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Link de indicação de clientes
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus"></i>
                                    <h2>
                                         Estudo de potencial clientes para construtoras e asessorias
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus "></i>
                                    <h2>
                                        Consultores especializados em financiamento imobiliário
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                            <li>
                                <div class="head">
                                    <i class="icon-plus "></i>
                                    <h2>
                                        Condições especiais
                                    </h2>
                                </div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt molestiae voluptate expedita eos necessitatibus voluptatum nisi hic dolor error qui soluta odit neque, adipisci, nesciunt, consequuntur libero nemo asperiores eius.</p>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

        </section>
        <script>
            jQuery(function($){
                $(document).ready(function(){
                    $('.head').click(function(){
                        $('.table_sobre ul li p').slideUp();
                        $(this).parent().find('p').slideToggle();

                        $('.table_sobre ul li i').parent().find('i').removeClass("active");
                        $(this).parent().find('i').addClass("active");
                    });
                });
            })
        </script>

<?php include('footer.php') ?>