<?php include('header.php') ?>

    <!-- Área de simulação -->

    <div class="simulacao-home">
        <div class="container">
            <div class="coluna chamada">
                <h2>Tire seus plenos do papel</h2>
                <span>Faça agora uma simulacao do seu financiamento</span>
            </div>
            <div class="coluna formulario">
                <form action="">
                    <label for="imovel">Valor do imóvel</label>
                    <input type="text" name="valor-do-imovel">
                    <label for="financiamento">Valor do fincanciamento desejado</label>
                    <input type="text" name="valor-do-financiamento">
                    <input type="submit" value="Fazer Simulação">
                </form>
            </div>
        </div>
    </div>

    <!-- Sobre a Credipronto -->
    
    <div class="sobre">
       <div class="container">
            <div class="coluna descricao">
                <div class="heading">
                    <h2>Sobre a CrediPronto</h2>
                </div>
                <div class="info">
                    <p>O itaú, maior intituição financeira do hemisfério sul, e a Lopes, a maior imobiliária do Brasil, se uniram para trazer um conceito inovador: oferecer o <strong>fincanciamento imobiliário que você precisa</strong>. Assim surgiu a CrediPronto. A única empresa especializada em <strong>consultoria de financiamento imobiliário</strong> do país. Conheça todos os benefícios e veja o porquê de fazer o financiamento do seu imível com a CrediPronto.</p>
                    <button class="saiba-mais">
                        <i class="icon-arrow-right"></i> Saiba Mais
                    </button>
                </div>
            </div>
            <div class="coluna video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/xW2DPUVZttg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <!-- Depoimentos -->

    <div class="depoimentos">
        <div class="container">
            <div class="heading">
                <h2>Depoimentos de Cliente e Parceiros</h2>
            </div>
        </div>
            <div class="box-depoimentos">
                <div class="container">
                    <div class="item">
                        <div class="cliente">
                            <img src="img/cliente.jpg" alt="">
                        </div>
                        <div class="depoimento">
                            <div class="titulo"><h3>Cliente 2</h3></div>
                            <div class="conteudo">
                            <i class="icon-quote"></i>
                            Realizamos neste último mês a compra do nosso primeiro imóvel através do financiamento junto à Credipronto e ficamos surpreendidos
                            com a qualidade do serviço prestado. A empresa superou nossas expectativas em questões de praticidade e agilidade na análide
                            e rápida aprovação do financiamento, sem burocracia demais e com tacas competitivas que, com toda certeza, fizeram a diferença
                            e viabilizaram o fechamento do negóco e a realização deste grande sonho. No quesito atendimento, não podemos deixar de parabenizar e agradecer à Joana Vitale e Fábio de Azebedo
                            pelo atendimento totalmente diferenciado que nos foi prestado, sempre com muito empenho e seriedade, ambos foram atenciosos e pacientes
                            ao conduzir nossa negociação desde o primeiro contato até a conclusão, nos auxiliando no que foi preciso e posicionando passo a passo sobre como
                            estava o andamento do processo, o que nos deixou tranquilos neste momento tão importante. Estamos muito satisfeitos e recomendamos a credipronto!
                            Parabéns à toda a equipe!
                            </div>
                            <div class="nome"><h4>Nome</h4></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Últimas notícias -->

    <div class="ultimas-noticias">
        <div class="container">
            <div class="titulo">
                <h2>Últimas matérias do blog</h2>
            </div>
            <div class="conteudo">
                <?php $json_data = file_get_contents("https://www.credipronto.com.br/blog/wp-json/wp/v2/posts");
                $json_data = json_decode($json_data, true);
                foreach ($json_data as $post => $key){
                if ($post <= 2){
                    echo '<div class="item">';
                    
                    $conteudo = $key['excerpt']['rendered'];
                    $conteudo = mb_convert_encoding($conteudo, "UTF-8");
                    $img = $json_data = file_get_contents($key['_links']['wp:featuredmedia'][0]['href']);
                    $img = json_decode($img, true);
                    
                    $img = $img['media_details']['sizes']['full']['source_url'];
                  
                echo '<img src="'.$img.'">';
                echo '<a href="'.$key['link'].'" target="_new"><h1>' .$key['title']['rendered'].'</h1></a>';
                echo '<div class="descricao">' . mb_convert_encoding($conteudo, "UTF-8") .'</div>';
                echo '</div>';
                }
                }
                ?>
            </div>
            <div class="saiba-mais"><a href="https://www.credipronto.com.br/blog" target="_new">Outros Posts</a></div>
        </div>
    </div>

     <!-- Estatísticas -->

     <div class="estatisticas">
         <div class="container">
            <ul>
                <li>
                    <span class="counter">20000</span>
                    <p>Contatos Emitidos</p>
                </li>
                <li>
                    <span class="counter">7</span>
                    <p>Bilhões em Financiamento</p>
                </li>
                <li>
                    <span class="counter">400</span>
                    <p>Parceiros</p>
                </li>
                <li>
                    <span class="counter">50</span>
                    <p>Cidades em Atuação</p>
                </li>
            </ul>
        </div>
     </div>
    
     <?php include('footer.php') ?>