  <!-- Rodape -->
     <footer id="rodape">
         <div class="container">
            <div class="info">
                <div class="coluna">
                    <div class="heading">
                        A Crediponto
                    </div>
                    <ul>
                        <li><a href="<?php echo $root . 'sobre' ?>">Quem Somos</a></li>
                        <li><a href="<?php echo $root . 'parceiros' ?>">Parceiros</a></li>
                        <li><a href="<?php echo $root . 'trabalhe-conosco' ?>">Trabalhe Conosco</a></li>
                        <li><a href="<?php echo $root . 'politica-de-privacidade' ?>">Política de Privacidade</a></li>
                        <li><a href="<?php echo $root . 'informativo-bacen' ?>">Informativo Bacen - Nº 4571</a></li>
                    </ul>
                </div>
                <div class="coluna">
                    <div class="heading">
                        Produtos
                    </div>
                    <ul>
                        <li><a href="<?php echo $root . 'financiamento' ?>">Financiamento</a></li>
                        <li><a href="<?php echo $root . 'emprestimos' ?>">Empréstimos</a></li>
                    </ul>
                </div>
                <div class="coluna contato">
                    <div class="heading">
                        Contato
                    </div>
                    <ul>
                        <li>
                            <span>Demais Localidades</span>
                            <div class="numero">
                                <i class="icon-telephone"></i> 3003 1144
                            </div>
                        </li>
                        <li>
                            <span>WhatsApp</span>
                            <div class="numero">
                                <i class="icon-whatsapp"></i> (11)99161 8296
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="coluna">
                    <div class="heading">
                        Redes Sociais
                    </div>
                    <ul class="social">
                        <li><i class="icon-facebook"></i></li>
                        <li><i class="icon-youtube-alt"></i></li>
                        <li><i class="icon-linkedin"></i></li>
                        <li><i class="icon-gplus"></i></li>
                    </ul>
                    <div class="heading">
                        Baixe Nosso Aplicativo
                    </div>
                    <div class="download">
                        <img src="img/play-store.png" alt="">
                        <img src="img/app-store.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p>2018. Todos os direitos reservados.</p>
            </div>
        </div>
     </footer>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>

<script>
    jQuery(function($){
        $('.login').click(function(event){
            event.preventDefault();
            $('.box').slideToggle();
        })
    })


</script>


<script>
    jQuery(function($){
        $(window).on('scroll', function() {
            scrollPosition = $(this).scrollTop();
            if (scrollPosition >= 80) {
                $("header").addClass('fixo');
            }
            else if(scrollPosition <= 80 ) {
                $("header").removeClass('fixo');
            }
        });
    })
</script>

</html>