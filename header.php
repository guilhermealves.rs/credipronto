<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Credipronto</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://file.myfontastic.com/p4uUXPejECK4vhCgLyCmka/icons.css" rel="stylesheet">
    <script>
        jQuery(document).ready(function( $ ) {
            $('.counter').counterUp({
                delay: 20,
                time: 2000
            });
        });
    </script>
<?php 
$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/credipronto/';
?>
</head>
<body>
    <!-- Cabeçalho -->
    <header>
        <div class="container">
            <div class="logo">
                <a href="<?php echo $root ?>"><img src="img/logo.png" alt="Credipronto"></a>
            </div>
            <div class="menu">
                <ul>
                    <li><a href="<?php echo $root . 'simulador' ?>">Simulador</a></li>
                    <li><a href="<?php echo $root . 'produtos' ?>">Produtos</a></li>
                    <li><a href="<?php echo $root . 'parceiros' ?>">Parceiros</a></li>
                    <li><a href="<?php echo $root . 'blog' ?>">Blog</a></li>
                    <li><a href="<?php echo $root . 'contato' ?>">Contato</a></li>
                    <li><a href="<?php echo $root . 'sobre' ?>">Sobre</a></li>
                    <li class="login">Login
                        <i class="icon-user"></i>
                        <div class="submenu">
                            <div class="box">
                                <ul>
                                    <li>Parceiros</li>
                                    <li>Funcionários</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>