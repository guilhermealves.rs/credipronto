<?php include('header.php') ?>

    <div class="page_title" style="background-image:url(img/header_parceiros.jpg)">
        <div class="container">
            <div class="left">
                <h1>Contato</h1>
            </div>
        </div>
    </div>

    <section class="container">
        <div class="content_parceiros">
            <div class="left">
                <h1>Entre em contato conosco</h1>
                    <form action="" class="form_credipronto">
                    <div class="content">
                        <div class="c6">
                            <input type="text" class="input" placeholder="Nome da Empresa">
                        </div>
                        <div class="c6">
                            <input type="text" class="input" placeholder="CNPJ">
                        </div>

                        <div class="c6">
                            <input type="text" class="input" placeholder="Nome do Responsável">
                        </div>
                        <div class="c6">
                            <input type="text" class="input" placeholder="CPF">
                        </div>

                        <div class="c4">
                            <input type="text" class="input" placeholder="CEP">
                        </div>
                        <div class="c4">
                            <input type="text" class="input" placeholder="E-mail">
                        </div>
                        <div class="c4">
                            <input type="text" class="input" placeholder="Telefone">
                        </div>

                        <div class="c6">
                            <select name="" id="">
                                <option value="" selected>Como fico sabendo da CrediPronto?</option>
                                <option value="">Google</option>
                                <option value="">Google</option>
                                <option value="">Google</option>
                            </select>
                        </div>
                        <div class="c6">

                        </div>
                    </div>
                    <div class="submit_container">
                        <button class="submit" type="submit" name="button">QUERO SER PARCEIRO</button>
                    </div>
                </form>
            </div>


                <div class="right">
                        <div class="infos">
                            <p>Rua Estados Unidos, 2031,</p>
                            <p>Jardim América, São Paulo - SP</p>
                            <p>(11) 3500-5970(Grande São Paulo)</p>
                            <p>(21)4042-0988(Rio de Janeiro)</p>
                        </div>

                        <ul class="social">
                            <li><i class="icon-facebook"></i></li>
                            <li><i class="icon-youtube-alt"></i></li>
                            <li><i class="icon-instagram"></i></li>
                            <li><i class="icon-linkedin"></i></li>
                            <li><i class="icon-gplus"></i></li>
                        </ul>

                        <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.122970352126!2d-46.67502388502228!3d-23.56402658468145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce577effa1d331%3A0xa8801d764b304e70!2sR.+Estados+Unidos%2C+2031+-+Jardim+America%2C+S%C3%A3o+Paulo+-+SP%2C+01427-002!5e0!3m2!1spt-BR!2sbr!4v1527010024782" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>

                </div>
            </div>

        </section>
        <script>
            jQuery(funtion($){
                $(document).ready(function(){
                    $('.head').click(function(){
                        $('.table_sobre ul li p').slideUp();
                        $(this).parent().find('p').slideToggle();

                        $('.table_sobre ul li i').parent().find('i').removeClass("active");
                        $(this).parent().find('i').addClass("active");
                    });
                });
            })
        </script>

<?php include('footer.php') ?>